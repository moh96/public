package com.example.maf.wifie.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.maf.wifie.R;

import java.util.ArrayList;

public class EarningListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<String> earningClient;
    private ArrayList<String> earningAmount;
    private ArrayList<String> earningDate;

    public EarningListAdapter(Context context, ArrayList<String> earningClient, ArrayList<String> earningAmount, ArrayList<String> earningDate) {
        this.context = context;
        this.earningClient = earningClient;
        this.earningAmount = earningAmount;
        this.earningDate = earningDate;
    }

    @Override
    public int getCount() {
        return earningClient.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        final View result;
        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.earning_list_item, parent, false);
            viewHolder.earningClient = convertView.findViewById(R.id.earningClient);
            viewHolder.earningAmount = convertView.findViewById(R.id.earningAmount);
            viewHolder.earningDate = convertView.findViewById(R.id.earningDate);

            result = convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        viewHolder.earningClient.setText(earningClient.get(position));
        viewHolder.earningAmount.setText(earningAmount.get(position));
        viewHolder.earningDate.setText(earningDate.get(position));

        return result;
    }

    private static class ViewHolder {

        TextView earningClient;
        TextView earningAmount;
        TextView earningDate;

    }
}
