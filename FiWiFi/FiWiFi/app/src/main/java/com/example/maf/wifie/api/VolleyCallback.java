package com.example.maf.wifie.api;

public interface VolleyCallback {
    void onSuccessResponse(String response);
    void onErrorResponse(String response);
}
