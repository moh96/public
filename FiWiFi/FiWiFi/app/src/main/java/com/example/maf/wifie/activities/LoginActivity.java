package com.example.maf.wifie.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.dd.processbutton.iml.ActionProcessButton;
import com.example.maf.wifie.R;
import com.example.maf.wifie.api.APIResponse;
import com.example.maf.wifie.api.APIService;
import com.example.maf.wifie.api.VolleyCallback;
import com.example.maf.wifie.api.APIError;
import com.example.maf.wifie.helpers.ProgressButton;
import com.example.maf.wifie.helpers.SharedPref;

import java.util.HashMap;
import java.util.Map;

import static com.example.maf.wifie.api.APIConfig.POST;

public class LoginActivity extends AppCompatActivity {

    private EditText editTextEmail;
    private EditText editTextPassword;
    private TextView textViewError;
    private ActionProcessButton btnLogin;
    private ProgressButton progressButton;
    private SharedPref sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);
        textViewError = findViewById(R.id.textViewError);
        btnLogin =  findViewById(R.id.btnLogin);

        sharedPref = new SharedPref(getApplicationContext());
        progressButton = new ProgressButton(btnLogin);

        btnLogin.setOnClickListener(v -> {
            userLogin();
        });
    }

    private void userLogin() {

        //start button progress
        progressButton.start();
        inputSetEnabled(false);

        //defining the api service
        APIService apiService = new APIService(getApplicationContext());
        apiService.setMethod(POST);
        apiService.setUrl("login");
        apiService.setParams(getLoginParams());

        apiService.sendRequest(new VolleyCallback() {
            @Override
            public void onSuccessResponse(String response) {

                //end button progress
                progressButton.success();
                inputSetEnabled(true);

                APIResponse apiResponse = new APIResponse(response);

                //save user to shared preference
                sharedPref.saveUser(apiResponse.getUserId(), apiResponse.getUserName(), apiResponse.getToken());

                Intent intent = new Intent(getApplication(), ClientActivity.class);
                intent.putExtra("firstTimeLogin",true);
                startActivity(intent);
                finish();
            }

            @Override
            public void onErrorResponse(String response) {
                APIError apiError = new APIError(response);

                editTextEmail.setError(apiError.getErrors().get("email"));
                editTextPassword.setError(apiError.getErrors().get("password"));
                textViewError.setText(apiError.getMessage());

                //end button progress
                progressButton.error();
                inputSetEnabled(true);
            }
        });

    }

    private void inputSetEnabled(boolean enabled){
        btnLogin.setEnabled(enabled);
        editTextEmail.setEnabled(enabled);
        editTextPassword.setEnabled(enabled);
    }

    private Map<String, String> getLoginParams() {

        String email = editTextEmail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();

        Map<String, String> params = new HashMap<String, String>();
        params.put("email", email);
        params.put("password", password);
        return params;
    }

}