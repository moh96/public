package com.example.maf.wifie.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.maf.wifie.R;

import java.util.ArrayList;

public class WifiListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<String> wifiName;
    private ArrayList<String> wifiPrice;
    private ArrayList<Integer> wifiIcons;

    public WifiListAdapter(Context context, ArrayList<String> wifiName, ArrayList<String> wifiType, ArrayList<Integer> wifiIcons) {
        this.context = context;
        this.wifiName = wifiName;
        this.wifiPrice = wifiType;
        this.wifiIcons = wifiIcons;
    }

    @Override
    public int getCount() {
        return wifiName.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint({"CutPasteId", "SetTextI18n"})
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        final View result;
        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.wifi_list_item, parent, false);
            viewHolder.wifiName = convertView.findViewById(R.id.wifi_name);
            viewHolder.wifiPrice = convertView.findViewById(R.id.wifi_price);
            viewHolder.wifiIcon = convertView.findViewById(R.id.wifi_icon);

            result = convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        viewHolder.wifiName.setText(wifiName.get(position));
        viewHolder.wifiPrice.setText(wifiPrice.get(position));
        viewHolder.wifiIcon.setImageResource(wifiIcons.get(position));

        return result;
    }

    private static class ViewHolder {

        TextView wifiName;
        TextView wifiPrice;
        ImageView wifiIcon;

    }
}

