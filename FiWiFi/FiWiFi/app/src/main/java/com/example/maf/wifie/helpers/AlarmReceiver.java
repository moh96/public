package com.example.maf.wifie.helpers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.example.maf.wifie.config.WifiConfig;

public class AlarmReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent arg1) {

        Toast.makeText(context, "Thank you for using FiWiFi!", Toast.LENGTH_SHORT).show();;
        WifiConfig wifiConfig = new WifiConfig(context);
        wifiConfig.disconnectWifi();

    }

}