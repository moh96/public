package com.example.maf.wifie.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.maf.wifie.R;
import com.example.maf.wifie.adapters.ClientListAdapter;
import com.example.maf.wifie.api.APIResponse;
import com.example.maf.wifie.api.APIService;
import com.example.maf.wifie.api.VolleyCallback;
import com.example.maf.wifie.config.WifiConfig;
import com.example.maf.wifie.helpers.SharedPref;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import info.whitebyte.hotspotmanager.ClientScanResult;
import info.whitebyte.hotspotmanager.WifiApManager;

import static com.example.maf.wifie.api.APIConfig.POST;

public class ProviderActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    private ImageView imageViewHotpost;
    private EditText editTextPrice;
    private WifiConfig wifiConfig;
    private SharedPref sharedPref;
    private WifiApManager wifiApManager;
    private ListView clientsList;
    private TextView hotspotDescription;
    private Boolean hotspotEnabled = false;
    private LinearLayout linearLayoutPrice;

    private ArrayList<String> clientNames;
    private ArrayList<String> IPAddresses;
    private ArrayList<String> MACAddresses;
    private ArrayList<String> PaymentInfo;
    private ClientListAdapter clientListAdapter;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider);

        //change action bar title
        Objects.requireNonNull(getSupportActionBar()).setTitle("Provider");

        imageViewHotpost = findViewById(R.id.imageViewHotpost);
        editTextPrice = findViewById(R.id.editTextPrice);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        clientsList = findViewById(R.id.clientList);
        hotspotDescription = findViewById(R.id.hotspotDescription);
        linearLayoutPrice = findViewById(R.id.linearLayoutPrice);

        sharedPref = new SharedPref(getApplicationContext());
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        wifiApManager = new WifiApManager(getApplicationContext());

        clientNames = new ArrayList<>();
        IPAddresses = new ArrayList<>();
        MACAddresses = new ArrayList<>();
        PaymentInfo = new ArrayList<>();

        clientListAdapter = new ClientListAdapter(ProviderActivity.this, clientNames, IPAddresses, MACAddresses, PaymentInfo);
        clientsList.setAdapter(clientListAdapter);

        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                menuItem -> {
                    // set item as selected to persist highlight
                    menuItem.setChecked(true);
                    // close drawer when item is tapped
                    mDrawerLayout.closeDrawers();
                    switch (menuItem.getItemId()) {
                        case R.id.nav_fiwifi:
                            startActivity(new Intent(getApplication(), ClientActivity.class));
                            finish();
                            break;

                        case R.id.nav_earnings:
                            startActivity(new Intent(getApplication(), EarningActivity.class));
                            break;

                        case R.id.nav_logout:
                            startActivity(new Intent(getApplication(), RegisterActivity.class));
                            sharedPref.removeUser();
                            finish();
                            break;
                    }
                    return true;
                });

        navigationView.setCheckedItem(R.id.nav_provider);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        setSwitchImageState();

        imageViewHotpost.setOnClickListener(v -> {
            if (!hotspotEnabled) {
                if (permissions()) {
                    startHotspot(getPriceInput());
                    return;
                }
            }
            stopHotspot();

        });

        FloatingActionButton fabRefresh = findViewById(R.id.fabRefresh);
        fabRefresh.setOnClickListener(view -> {
            Snackbar.make(view, "Searching for clients", Snackbar.LENGTH_LONG).show();
            scan();
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 200: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startHotspot(getPriceInput());
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);

    }

    private void stopHotspot() {
        hotspotEnabled = false;
        imageViewHotpost.setImageResource(R.drawable.hotspot_disabled);
        hotspotDescription.setText(R.string.tethering_on);
        linearLayoutPrice.setVisibility(View.VISIBLE);

        wifiApManager.setWifiApEnabled(null, false);
    }

    private void startHotspot(String price) {

        if (price == null) {
            return;
        }

        hotspotEnabled = true;
        imageViewHotpost.setImageResource(R.drawable.hotspot_enabled);
        hotspotDescription.setText(R.string.tethering_off);
        linearLayoutPrice.setVisibility(View.GONE);

        //set wifi ssid and password
        wifiConfig = new WifiConfig(ProviderActivity.this);
        wifiConfig.setSSID(sharedPref.getUserId(), price);
        wifiConfig.setPASSWORD();

        //set wifiApManager configuration and turn hotspot on
        wifiApManager.setWifiApEnabled(wifiConfig.getConfig(), true);

    }

    private boolean permissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.System.canWrite(getApplicationContext())) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, 200);
                return false;
            }
        }
        return true;
    }

    private String getPriceInput() {
        String price = editTextPrice.getText().toString();
        //check if input price is empty
        if (price.trim().length() == 0) {
            editTextPrice.setError("This field is required");
            return null;
        }
        return editTextPrice.getText().toString();
    }

    private void getUserDetails(ArrayList<String> macAddresses) {

        String jsonMacAddresses = new Gson().toJson(macAddresses);

        Map<String, String> params = new HashMap<>();
        params.put("macAddress", jsonMacAddresses);
        APIService apiService = new APIService(getApplicationContext());
        apiService.setMethod(POST);
        apiService.setUrl("user");
        apiService.setParams(params);
        apiService.setToken(sharedPref.getUserToken());

        apiService.sendRequest(new VolleyCallback() {
            @Override
            public void onSuccessResponse(String response) {
                APIResponse apiResponse = new APIResponse(response);
                try {
                    setClientListAdapterFromServer(apiResponse.getUserNames(), apiResponse.getPaymentInfo());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(String response) {
                Log.e("response", response);
            }
        });

    }

    private void scan() {
        //            wifiApManager.getWifiApState();
        wifiApManager.getClientList(false, this::setClientListAdapter);
    }

    private void setClientListAdapter(ArrayList<ClientScanResult> clients) {
        clientNames.clear();
        IPAddresses.clear();
        MACAddresses.clear();
        PaymentInfo.clear();
        clientListAdapter.notifyDataSetChanged();

        for (int i = 0; i < clients.size(); i++) {
            ClientScanResult clientScanResult = clients.get(i);
            Log.i("clients", clientScanResult.toString());

            if (!clientScanResult.getHWAddr().equals("00:00:00:00:00:00")) {
                clientNames.add("Unknown");
                IPAddresses.add(clientScanResult.getIpAddr());
                MACAddresses.add(clientScanResult.getHWAddr());
                PaymentInfo.add("None");
            }
        }

        clientListAdapter.notifyDataSetChanged();
        Log.i("macaddresses", MACAddresses.toString());

        if (MACAddresses == null) {
            Toast.makeText(this, "No clients found", Toast.LENGTH_SHORT).show();
            return;
        }
        getUserDetails(MACAddresses);
    }

    private void setClientListAdapterFromServer(JSONArray jsonArrayUserNames, JSONArray jsonArrayPaymentInfo) throws JSONException {
        clientNames.clear();
        PaymentInfo.clear();

        for (int i = 0; i < jsonArrayUserNames.length(); i++) {
            clientNames.add((String) jsonArrayUserNames.get(i));
            PaymentInfo.add((String) jsonArrayPaymentInfo.get(i));
        }
        clientListAdapter.notifyDataSetChanged();
    }

    public void setSwitchImageState() {
        if (this.wifiApManager.isWifiApEnabled()) {
            imageViewHotpost.setImageResource(R.drawable.hotspot_enabled);
            linearLayoutPrice.setVisibility(View.GONE);
            hotspotDescription.setText(R.string.tethering_off);
            hotspotEnabled = true;
            return;
        }
        this.imageViewHotpost.setImageResource(R.drawable.hotspot_disabled);
        linearLayoutPrice.setVisibility(View.VISIBLE);
        hotspotDescription.setText(R.string.tethering_on);
        hotspotEnabled = false;
    }

    @Override
    public void onResume() {
        setSwitchImageState();
        navigationView.setCheckedItem(R.id.nav_provider);

        super.onResume();
    }
}
