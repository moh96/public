package com.example.maf.wifie.api;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.maf.wifie.activities.RegisterActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class APIResponse {

    private String totalEarnings;
    private JSONArray earnings;
    private Boolean success;
    private String message;
    private JSONObject data;
    private String token;
    private String userId;
    private String userName;
    private JSONArray userNames;
    private JSONArray paymentInfo;

    public APIResponse(String response) {
        if (response.equals("")) {
            return;
        }

        try {
            JSONObject json = new JSONObject(response);

            this.success = json.getBoolean("success");
            this.message = json.getString("message");
            this.data = json.getJSONObject("data");

            try {
                this.token = data.getString("token");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                this.userId = data.getString("userId");

            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                this.userName = data.getString("userName");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                this.userNames = data.getJSONArray("userNames");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                this.earnings = data.getJSONArray("earnings");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                this.paymentInfo = data.getJSONArray("payments");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                this.totalEarnings = data.getString("total");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public Boolean getSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public JSONObject getData() {
        return data;
    }

    public String getToken() {
        return token;
    }

    public String getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public JSONArray getUserNames() {
        return userNames;
    }

    public JSONArray getPaymentInfo() {
        return paymentInfo;
    }

    public JSONArray getEarnings() {
        return earnings;
    }

    public String getTotalEarnings() {
        return totalEarnings;
    }
}
