package com.example.maf.wifie.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.dd.processbutton.iml.ActionProcessButton;
import com.example.maf.wifie.R;
import com.example.maf.wifie.api.APIResponse;
import com.example.maf.wifie.api.APIService;
import com.example.maf.wifie.api.VolleyCallback;
import com.example.maf.wifie.api.APIError;
import com.example.maf.wifie.helpers.ProgressButton;
import com.example.maf.wifie.helpers.SharedPref;

import java.util.HashMap;
import java.util.Map;

import static com.example.maf.wifie.api.APIConfig.POST;

public class RegisterActivity extends AppCompatActivity {

    private EditText editTextName;
    private EditText editTextEmail;
    private EditText editTextPassword;
    private EditText editTextPasswordConfirm;
    private TextView textViewLogin;
    private ActionProcessButton btnRegister;
    private ProgressButton progressButton;
    private SharedPref sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        editTextName = findViewById(R.id.editTextName);
        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);
        editTextPasswordConfirm = findViewById(R.id.editTextPasswordConfirm);
        btnRegister = findViewById(R.id.btnRegister);
        btnRegister.setMode(ActionProcessButton.Mode.ENDLESS);
        textViewLogin = findViewById(R.id.textViewLogin);

        sharedPref = new SharedPref(getApplicationContext());
        progressButton = new ProgressButton(btnRegister);

        if (sharedPref.getUserToken() != null) {
            startActivity(new Intent(getApplication(), ClientActivity.class));
            finish();
        }

        btnRegister.setOnClickListener(v -> userRegister());

        textViewLogin.setOnClickListener(v -> {
            startActivity(new Intent(getApplication(), LoginActivity.class));
        });

    }

    private void userRegister() {

        //start button progress
        progressButton.start();
        inputSetEnabled(false);

        //defining the api service
        APIService apiService = new APIService(getApplicationContext());
        apiService.setMethod(POST);
        apiService.setUrl("register");
        apiService.setParams(getRegisterParams());

        apiService.sendRequest(new VolleyCallback() {
            @Override
            public void onSuccessResponse(String response) {

                //end button progress
                progressButton.success();
                inputSetEnabled(true);

                APIResponse apiResponse = new APIResponse(response);

                //save user to shared preference
                sharedPref.saveUser(apiResponse.getUserId(), apiResponse.getUserName(), apiResponse.getToken());

                Intent intent = new Intent(getApplication(), ClientActivity.class);
                intent.putExtra("firstTimeLogin",true);
                startActivity(intent);
            }

            @Override
            public void onErrorResponse(String response) {

                APIError apiError = new APIError(response);

                editTextName.setError(apiError.getErrors().get("name"));
                editTextEmail.setError(apiError.getErrors().get("email"));
                editTextPassword.setError(apiError.getErrors().get("password"));

                //end button progress
                progressButton.error();
                inputSetEnabled(true);
            }
        });

    }

    private Map<String, String> getRegisterParams() {

        String name = editTextName.getText().toString().trim();
        String email = editTextEmail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();
        String passwordConfirm = editTextPasswordConfirm.getText().toString().trim();

        Map<String, String> params = new HashMap<String, String>();
        params.put("name", name);
        params.put("email", email);
        params.put("password", password);
        params.put("password_confirmation", passwordConfirm);

        return params;
    }

    private void inputSetEnabled(boolean enabled) {
        btnRegister.setEnabled(enabled);
        editTextName.setEnabled(enabled);
        editTextEmail.setEnabled(enabled);
        editTextPassword.setEnabled(enabled);
        editTextPasswordConfirm.setEnabled(enabled);
    }

}
