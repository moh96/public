package com.example.maf.wifie.api;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import static com.example.maf.wifie.api.APIConfig.BASE_URL;


public class APIService {

    private Context context;
    private Map<String, String> params;
    private String url;
    private int method;
    private String token;
    VolleyCallback callback = null;

    public APIService(Context context) {
        this.context = context;
    }

    public void sendRequest(final VolleyCallback callback) {

        RequestQueue queue = Volley.newRequestQueue(context);

        StringRequest sr = new StringRequest(method, BASE_URL + url,
                response -> {

                    Log.e("HttpClient", "success! response: " + response);
                    callback.onSuccessResponse(response);
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("HttpClient", "error: " + error.toString());

                        if (error instanceof NetworkError) {
                            Toast.makeText(context, "Cannot connect to Internet...Please check your connection", Toast.LENGTH_SHORT).show();
                            callback.onErrorResponse("");

                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(context, "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();
                            callback.onErrorResponse("");

                        } else if (error instanceof ParseError) {
                            Toast.makeText(context, "Please try again after some time!", Toast.LENGTH_SHORT).show();
                            callback.onErrorResponse("");

                        } else if (error instanceof TimeoutError) {
                            Toast.makeText(context, "Connection TimeOut! Please check your internet connection.", Toast.LENGTH_SHORT).show();
                            callback.onErrorResponse("");
                        } else if (error instanceof ServerError) {
                            try {
                                String responseBody = new String(error.networkResponse.data, "utf-8");
                                JSONObject jsonObject = new JSONObject(responseBody);

                                callback.onErrorResponse(jsonObject.toString());

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                params.put("Accept", "application/json");

                if (token != null) {
                    params.put("Authorization", "Bearer " + token);

                }

                return params;
            }
        };

        queue.add(sr);

    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setMethod(int method) {
        this.method = method;
    }

}
