package com.example.maf.wifie.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.maf.wifie.R;
import com.example.maf.wifie.adapters.EarningListAdapter;
import com.example.maf.wifie.api.APIResponse;
import com.example.maf.wifie.api.APIService;
import com.example.maf.wifie.api.VolleyCallback;
import com.example.maf.wifie.helpers.SharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import static com.android.volley.Request.Method.GET;

public class EarningActivity extends AppCompatActivity {

    SharedPref sharedPref;
    private ArrayList<String> earningClients;
    private ArrayList<String> earningAmounts;
    private ArrayList<String> earningDates;
    private ListView earningList;
    private EarningListAdapter earningListAdapter;
    private TextView listViewTotalEarnings;
    private View footerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earning);

        earningClients = new ArrayList<>();
        earningAmounts = new ArrayList<>();
        earningDates = new ArrayList<>();
        sharedPref = new SharedPref(EarningActivity.this);
        earningListAdapter = new EarningListAdapter(EarningActivity.this, earningClients, earningAmounts, earningDates);

        footerView = ((LayoutInflater) Objects.requireNonNull(getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE))).
                inflate(R.layout.earning_list_footer, null, false);

        listViewTotalEarnings = footerView.findViewById(R.id.listViewTotalEarnings);

        earningList = findViewById(R.id.earningList);
        earningList.setAdapter(earningListAdapter);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        FloatingActionButton fabRefresh = findViewById(R.id.fabEarningRefresh);
        fabRefresh.setOnClickListener(view -> {
            getEarnings();
        });

        getEarnings();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private void getEarnings() {

        earningAmounts.clear();
        earningClients.clear();
        earningDates.clear();

        //defining the api service
        APIService apiService = new APIService(getApplicationContext());
        apiService.setMethod(GET);
        apiService.setUrl("earnings");
        apiService.setToken(sharedPref.getUserToken());

        apiService.sendRequest(new VolleyCallback() {
            @Override
            public void onSuccessResponse(String response) {

                APIResponse apiResponse = new APIResponse(response);
                clearAdapter();
                try {
                    setEarningAdapter(apiResponse.getEarnings());
                    earningList.removeFooterView(footerView);
                    listViewTotalEarnings.setText(apiResponse.getTotalEarnings());
                    earningList.addFooterView(footerView);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(String response) {

            }

        });

    }

    private void setEarningAdapter(JSONArray jsonArrayEarnings) throws JSONException {

        for (int i = 0; i < jsonArrayEarnings.length(); i++) {

            JSONObject data = new JSONObject(jsonArrayEarnings.get(i).toString());

            earningClients.add(data.getString("clientName"));
            earningAmounts.add(data.getString("amount") + "$");
            earningDates.add(data.getString("earnedAt"));
        }
        earningListAdapter.notifyDataSetChanged();
    }

    private void clearAdapter() {
        earningClients.clear();
        earningAmounts.clear();
        earningDates.clear();
        earningListAdapter.notifyDataSetChanged();
    }

}
