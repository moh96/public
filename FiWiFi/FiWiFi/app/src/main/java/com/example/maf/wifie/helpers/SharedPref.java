package com.example.maf.wifie.helpers;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class SharedPref {

    private Context context;
    private SharedPreferences.Editor editor;
    private SharedPreferences prefs;

    @SuppressLint("CommitPrefEdits")
    public SharedPref(Context context) {
        this.context = context;
        this.prefs = (context).getSharedPreferences("user", MODE_PRIVATE);
        this.editor = this.prefs.edit();

    }

    public void saveUser(String id, String name, String token) {
        editor.putString("id", id);
        editor.putString("name", name);
        editor.putString("token", token);
        editor.apply();
    }

    public void removeUser() {
        editor.putString("id", null);
        editor.putString("name", null);
        editor.putString("token", null);
        editor.apply();
    }

    public String getUserId() {
        return prefs.getString("id", null);
    }

    public String getUserName() {
        return prefs.getString("name", null);
    }

    public String getUserToken() {
        return prefs.getString("token", null);
    }

}
