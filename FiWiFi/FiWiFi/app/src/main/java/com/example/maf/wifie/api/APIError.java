package com.example.maf.wifie.api;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class APIError {

    private Boolean success;
    private String message;
    private Map<String, String> errors = new HashMap<>();

    public APIError(String response) {
        if (response.equals("")) {
            return;
        }

        try {
            JSONObject json = new JSONObject(response);

            //get response data
            this.success = json.getBoolean("success");
            this.message = json.getString("message");

            JSONObject data = json.getJSONObject("data");

            //fill errors in a HashMap
            if (data.has("name")) {
                JSONArray nameError = data.getJSONArray("name");
                this.errors.put("name", nameError.getString(0));
            }
            if (data.has("email")) {
                JSONArray emailError = data.getJSONArray("email");
                this.errors.put("email", emailError.getString(0));
            }
            if (data.has("password")) {
                JSONArray passwordError = data.getJSONArray("password");
                this.errors.put("password", passwordError.getString(0));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public Boolean getSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public Map<String, String> getErrors() {
        return errors;
    }
}
