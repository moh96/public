package com.example.maf.wifie.activities;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.dd.processbutton.iml.ActionProcessButton;
import com.example.maf.wifie.R;
import com.example.maf.wifie.adapters.WifiListAdapter;
import com.example.maf.wifie.api.APIService;
import com.example.maf.wifie.api.VolleyCallback;
import com.example.maf.wifie.config.WifiConfig;
import com.example.maf.wifie.helpers.ProgressButton;
import com.example.maf.wifie.helpers.SharedPref;
import com.thanosfisherman.wifiutils.WifiUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.example.maf.wifie.api.APIConfig.POST;

public class ClientActivity extends AppCompatActivity {

    private ActionProcessButton btnScan;
    private ListView wifiList;

    private ArrayList<Integer> wifiIcons;
    private ArrayList<String> wifiNames;
    private ArrayList<String> wifiPrices;
    private ArrayList<String> providerIDs;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    private SharedPref sharedPref;
    private ProgressButton progressButton;
    private WifiConfig wifiConfig;
    private String providerID;
    private String wifiPrice;
    private String wifiName;
    private WifiListAdapter wifiListAdapter;
    private ProgressDialog progressDialog;
    private int networkId = 0;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client);

        //change action bar title
        Objects.requireNonNull(getSupportActionBar()).setTitle("FiWiFi?");


        btnScan = findViewById(R.id.btnScan);
        wifiList = findViewById(R.id.wifiList);

        wifiConfig = new WifiConfig(ClientActivity.this);
        sharedPref = new SharedPref(getApplicationContext());
        progressButton = new ProgressButton(btnScan);
        wifiIcons = new ArrayList<>();
        wifiNames = new ArrayList<>();
        wifiPrices = new ArrayList<>();
        providerIDs = new ArrayList<>();
        progressDialog = new ProgressDialog(ClientActivity.this);

        wifiListAdapter = new WifiListAdapter(ClientActivity.this, wifiNames, wifiPrices, wifiIcons);
        wifiList.setAdapter(wifiListAdapter);

        boolean permissionGranted = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            Boolean firstTimeLogin = extras.getBoolean("firstTimeLogin");
            if (firstTimeLogin) {
                storeMacAddress();
            }
        }

        wifiList.setOnItemClickListener((adapterView, view, i, l) -> {

            Pattern pattern = Pattern.compile("\\d+");
            Matcher matcher = pattern.matcher(wifiPrices.get(i));
            if (matcher.find()) {
                progressDialog.setMessage("Connecting to Wifi...");
                progressDialog.show();

                this.wifiPrice = wifiPrices.get(i);
                this.wifiName = wifiNames.get(i) + "-" + this.wifiPrice;
                this.providerID = providerIDs.get(i);

                connectToWifi(wifiNames.get(i) + "-" + matcher.group(0));

                new Handler().postDelayed(() -> {
                    checkConnectResult(wifiName);
                }, 3200);
            }

        });

        btnScan.setOnClickListener(v -> {
            if (permissionGranted && isGpsEnabled()) {
                scanWifi();
            } else {
                ActivityCompat.requestPermissions(ClientActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 200);
            }
        });

        //navigation view configuration
        mDrawerLayout = findViewById(R.id.drawer_layout);
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                menuItem -> {
                    // set item as selected to persist highlight
                    menuItem.setChecked(true);
                    // close drawer when item is tapped
                    mDrawerLayout.closeDrawers();

                    // Add code here to update the UI based on the item selected
                    // For example, swap UI fragments here
                    switch (menuItem.getItemId()) {
                        case R.id.nav_provider:
                            startActivity(new Intent(getApplication(), ProviderActivity.class));
                            finish();
                            break;

                        case R.id.nav_earnings:
                            startActivity(new Intent(getApplication(), EarningActivity.class));
                            break;

                        case R.id.nav_logout:
                            startActivity(new Intent(getApplication(), RegisterActivity.class));
                            sharedPref.removeUser();
                            finish();
                            break;
                    }
                    return true;
                });
        navigationView.setCheckedItem(R.id.nav_fiwifi);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 200: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (isGpsEnabled()) {
                        scanWifi();
                        return;
                    }
                    buildAlertMessageNoGps();
                }
            }
        }
    }


    public boolean isGpsEnabled() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        return Objects.requireNonNull(manager).isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void setWifiListAdapter(List<ScanResult> results) {
        wifiIcons.clear();
        wifiNames.clear();
        wifiPrices.clear();
        providerIDs.clear();
        wifiListAdapter.notifyDataSetChanged();

        for (int i = 0; i < results.size(); i++) {

            int wifiSignal = getWifiSignal(results.get(i).level);
            String wifiName = results.get(i).SSID;

            if (validSSID(wifiName)) {

                String[] arraySSID = wifiName.split("-");

                String providerID = arraySSID[2];
                String wifiPrice = arraySSID[3];
                wifiName = wifiName.replaceFirst("-\\d+$", "");

                String drawableIconName = "baseline_signal_wifi_" + wifiSignal;
                int wifiIconResID = getResources().getIdentifier(drawableIconName, "drawable", getPackageName());

                wifiIcons.add(wifiIconResID);
                wifiNames.add(wifiName);
                wifiPrices.add(wifiPrice);

                providerIDs.add(providerID);

            }
        }
        if (wifiNames.isEmpty()) {
            //end button progress
            progressButton.error();
        } else {
            wifiListAdapter.notifyDataSetChanged();
            //end button progress
            progressButton.success();
        }
    }

    private void connectToWifi(String ssid) {
        progressDialog.show();

        WifiConfig wifiConfig = new WifiConfig(ClientActivity.this);
        wifiConfig.connectToWifi(ssid, true);

        new Handler().postDelayed(
                () -> progressDialog.cancel(), 2800);

    }

    private void checkConnectResult(String name) {
        if (wifiConfig.getWifiName().equals("\"" + name + "\"")) {
            Toast.makeText(ClientActivity.this, "Connection successful", Toast.LENGTH_SHORT).show();

            //start payment activity
            Intent i = new Intent(ClientActivity.this, PaymentActivity.class);
            i.putExtra("wifiPrice", this.wifiPrice);
            i.putExtra("providerID", this.providerID);
            i.putExtra("wifiName", this.wifiName);
            i.putExtra("networkId", this.networkId);
            startActivity(i);

        } else {
            Toast.makeText(getApplicationContext(), "Cannot connect to WiFi. Try again!", Toast.LENGTH_SHORT).show();
        }
    }

    private void enableWifi() {
        WifiUtils.withContext(getApplicationContext()).enableWifi(this::checkWifiEnableResult);
    }

    private void checkWifiEnableResult(boolean isSuccess) {
        if (isSuccess)
            Toast.makeText(getApplicationContext(), "Wifi enabled", Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(getApplicationContext(), "Can't enable Wifi. Try again!", Toast.LENGTH_SHORT).show();
    }

    private void scanWifi() {
        //start button progress
        progressButton.start();

        enableWifi();
        WifiUtils.withContext(getApplicationContext()).scanWifi(this::getScanResults).start();
    }

    private void getScanResults(@NonNull final List<ScanResult> results) {
        if (results.isEmpty()) {
            progressButton.error();
            return;
        }
        setWifiListAdapter(results);
    }

    private int getWifiSignal(int level) {
        if (level > -50) {
            return 4;
        } else if (level > -60) {
            return 3;
        } else if (level > -70) {
            return 2;
        } else {
            return 1;
        }
    }

    public boolean validSSID(String ssid) {
        Pattern p = Pattern.compile("^FiWiFi-Provider-\\d+-\\d+$");
        Matcher m = p.matcher(ssid);
        return m.matches();
    }

    private void storeMacAddress() {
        String macAddress = wifiConfig.getMacAddress();

        Map<String, String> params = new HashMap<String, String>();
        params.put("macAddress", macAddress);

        APIService apiService = new APIService(getApplicationContext());
        apiService.setMethod(POST);
        apiService.setUrl("macAddress/store");
        apiService.setParams(params);
        apiService.setToken(sharedPref.getUserToken());

        apiService.sendRequest(new VolleyCallback() {
            @Override
            public void onSuccessResponse(String response) {
            }

            @Override
            public void onErrorResponse(String response) {
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (wifiListAdapter != null)
            wifiListAdapter.notifyDataSetChanged();
        navigationView.setCheckedItem(R.id.nav_fiwifi);
    }
}
