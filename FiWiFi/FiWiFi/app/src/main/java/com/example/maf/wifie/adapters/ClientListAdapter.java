package com.example.maf.wifie.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.maf.wifie.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ClientListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<String> clientNames;
    private ArrayList<String> IPAddresses;
    private ArrayList<String> MACAddresses;
    private ArrayList<String> PaymentInfo;

    public ClientListAdapter(Context context, ArrayList<String> clientNames,
                             ArrayList<String> IPAddresses, ArrayList<String> MACAddresses, ArrayList<String> PaymentInfo) {
        this.context = context;
        this.clientNames = clientNames;
        this.IPAddresses = IPAddresses;
        this.MACAddresses = MACAddresses;
        this.PaymentInfo = PaymentInfo;
    }

    @Override
    public int getCount() {
        return clientNames.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint({"CutPasteId", "SetTextI18n"})
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        final View result;
        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.client_list_item, parent, false);
            viewHolder.clientNames = convertView.findViewById(R.id.client_name);
            viewHolder.IPAddresses = convertView.findViewById(R.id.ip_address);
            viewHolder.MACAddresses = convertView.findViewById(R.id.mac_address);
            viewHolder.PaymentInfo = convertView.findViewById(R.id.textViewPaid);

            result = convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        viewHolder.clientNames.setText(clientNames.get(position));
        viewHolder.IPAddresses.setText(IPAddresses.get(position));
        viewHolder.MACAddresses.setText(MACAddresses.get(position));
        viewHolder.PaymentInfo.setText(PaymentInfo.get(position));

        if (PaymentInfo.get(position).equals("Paid")) {
            viewHolder.PaymentInfo.setTextColor(ContextCompat.getColor(context, R.color.green));
        }
        if (PaymentInfo.get(position).equals("Unpaid")) {
            viewHolder.PaymentInfo.setTextColor(ContextCompat.getColor(context, R.color.red));
        }

        return result;
    }

    private static class ViewHolder {

        TextView clientNames;
        TextView IPAddresses;
        TextView MACAddresses;
        TextView PaymentInfo;

    }
}

