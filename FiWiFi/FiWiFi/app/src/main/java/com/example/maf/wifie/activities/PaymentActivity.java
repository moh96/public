package com.example.maf.wifie.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.dd.processbutton.iml.ActionProcessButton;
import com.example.maf.wifie.R;
import com.example.maf.wifie.api.APIError;
import com.example.maf.wifie.api.APIResponse;
import com.example.maf.wifie.api.APIService;
import com.example.maf.wifie.api.VolleyCallback;
import com.example.maf.wifie.config.WifiConfig;
import com.example.maf.wifie.helpers.AlarmReceiver;
import com.example.maf.wifie.helpers.ProgressButton;
import com.example.maf.wifie.helpers.SharedPref;
import com.thanosfisherman.wifiutils.WifiUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.example.maf.wifie.api.APIConfig.POST;

public class PaymentActivity extends AppCompatActivity {

    private ActionProcessButton btnPay;
    private ProgressButton progressButton;
    private TextView textViewAmount;
    private String wifiPrice;
    private String wifiName;
    private String providerID;
    private SharedPref sharedPref;
    private Boolean paid;
    private PendingIntent pendingIntent;
    private AlarmManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        Intent alarmIntent = new Intent(this, AlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);

        btnPay = findViewById(R.id.btnPay);
        textViewAmount = findViewById(R.id.textViewAmount);

        progressButton = new ProgressButton(btnPay);
        sharedPref = new SharedPref(getApplicationContext());

        Intent i = getIntent();
        wifiPrice = i.getStringExtra("wifiPrice");
        providerID = i.getStringExtra("providerID");
        wifiName = i.getStringExtra("wifiName");

        textViewAmount.setText(wifiPrice);
        paid = false;

        btnPay.setOnClickListener(v -> pay());
    }

    private void pay() {

        String date = (String) DateFormat.format("yyyy-MM-dd HH:mm:ss", new Date());
        //start button progress
        progressButton.start();
        btnPay.setEnabled(false);

        Map<String, String> params = new HashMap<String, String>();
        params.put("provider_id", providerID);
        params.put("amount", wifiPrice);
        params.put("paid", "1");
        params.put("earned_at", date);

        //defining the api service
        APIService apiService = new APIService(getApplicationContext());
        apiService.setMethod(POST);
        apiService.setUrl("payment/update");
        apiService.setParams(params);
        apiService.setToken(sharedPref.getUserToken());

        apiService.sendRequest(new VolleyCallback() {
            @Override
            public void onSuccessResponse(String response) {

                //end button progress
                progressButton.success();
                btnPay.setEnabled(true);
                paid = true;

                turnWifiOffAfterHour();
                Toast.makeText(PaymentActivity.this, "Payment successful", Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onErrorResponse(String response) {

                //end button progress
                progressButton.error();
                btnPay.setEnabled(true);
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!paid) {
            WifiConfig wifiConfig = new WifiConfig(getApplicationContext());
            wifiConfig.disconnectWifi();
            Toast.makeText(this, "Payment Unsuccessful", Toast.LENGTH_SHORT).show();
        }
    }

    public void turnWifiOffAfterHour() {
        manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        Objects.requireNonNull(manager).setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() + AlarmManager.INTERVAL_HOUR,
                0, pendingIntent);
    }


}
