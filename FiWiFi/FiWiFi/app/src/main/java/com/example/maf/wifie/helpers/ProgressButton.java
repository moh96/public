package com.example.maf.wifie.helpers;

import com.dd.processbutton.iml.ActionProcessButton;
import com.example.maf.wifie.R;

public class ProgressButton {

    private ActionProcessButton button;

    public ProgressButton(ActionProcessButton button) {
        this.button = button;
        this.button.setMode(ActionProcessButton.Mode.ENDLESS);

    }

    public void start() {
        button.setProgress(50);
    }

    public void error() {
        button.setProgress(-1);

    }

    public void success() {
        button.setProgress(100);
    }


}
