package com.example.maf.wifie.config;

import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.widget.Toast;

import java.net.NetworkInterface;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import static android.content.Context.WIFI_SERVICE;

public class WifiConfig {

    private final Context context;
    private String date;
    private String SSID;
    private String PASSWORD;
    private int netId;
    WifiManager wifiManager;

    public WifiConfig(Context context) {
        this.context = context;
        this.date = getDate();
        wifiManager= (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
    }

    public String getSSID() {
        return SSID;
    }

    public String getPASSWORD() {
        return PASSWORD;
    }

    public void setSSID(String userId, String price) {
        this.SSID = "FiWiFi-Provider-" + userId + "-" + price;
    }

    public void setPASSWORD() {

        StringBuilder pass = new StringBuilder();
        String key = "@#$_&!$#$";
        String key2 = "POIUYTREW";

        for (int i = 0; i < date.length(); i++) {
            pass.append(key.charAt(i));
            pass.append(date.charAt(i));
            if (date.charAt(i) % 2 == 0) {
                pass.append(key2.charAt(i));
            }
        }

        pass.reverse();

        this.PASSWORD = pass.toString();
    }

    private String getDate() {

        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);

        return "" + day + month + year;
    }

    public WifiConfiguration getConfig() {

        WifiConfiguration wifiCon = new WifiConfiguration();

        wifiCon.SSID = SSID;
        wifiCon.preSharedKey = PASSWORD;
        wifiCon.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.SHARED);
        wifiCon.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
        wifiCon.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
        wifiCon.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);

        return wifiCon;
    }


    public String getMacAddress() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(Integer.toHexString(b & 0xFF)).append(":");
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

    public void connectToWifi(String ssid, boolean connect) {

        WifiConfiguration wifiConfiguration = new WifiConfiguration();
        this.setPASSWORD();
        wifiConfiguration.SSID = String.format("\"%s\"", ssid);
        if (connect) {
            wifiConfiguration.preSharedKey = String.format("\"%s\"", this.getPASSWORD());
        } else {
            wifiConfiguration.preSharedKey = String.format("\"%s\"", "********");
        }


        if (wifiManager != null) {
            wifiManager.disconnect();
            wifiManager.enableNetwork(wifiManager.addNetwork(wifiConfiguration), true);
            wifiManager.reconnect();
            wifiManager.removeNetwork(netId);
        }
    }

    public String getWifiName() {
        String ssid = null;

        WifiInfo wifiInfo = null;
        if (wifiManager != null) {
            wifiInfo = wifiManager.getConnectionInfo();
        }
        if (wifiInfo != null) {
            ssid = wifiInfo.getSSID();
        }
        return ssid;
    }

    public void disconnectWifi() {

        List<WifiConfiguration> list = Objects.requireNonNull(wifiManager).getConfiguredNetworks();
        for (WifiConfiguration i : list) {
            wifiManager.removeNetwork(i.networkId);
            wifiManager.saveConfiguration();
        }

    }
}
