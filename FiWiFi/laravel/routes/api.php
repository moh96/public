<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register','Api\RegisterController@register');
Route::post('login','Api\LoginController@login');

Route::group(['middleware' => ['auth:api']], function () {
    Route::post('macAddress/store','Api\MacAddressController@store');
    Route::post('macAddress/get','Api\MacAddressController@get');
    Route::post('user','Api\UserController@detailsByMacAddresses');
    Route::post('payment/update','Api\PaymentController@update');
    Route::get('earnings','Api\EarningController@get');
});
