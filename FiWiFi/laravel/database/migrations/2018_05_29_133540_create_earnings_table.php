<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEarningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('earnings', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('amount');
            $table->dateTime('earned_at');

            $table->unsignedInteger('client_id');
            $table->foreign('client_id')->references('id')->on('users');
            $table->unsignedInteger('provider_id');
            $table->foreign('provider_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    { Schema::table('earnings', function (Blueprint $table) {
        $table->dropForeign(['client_id']);
        $table->dropForeign(['provider_id']);
    });
        Schema::dropIfExists('earnings');
    }
}
