<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = ['client_id','provider_id','paid', 'amount'];

    public function client()
    {
        $this->belongsTo('User', 'client_id');
    }
    public function provider()
    {
        $this->belongsTo('User', 'provider_id');
    }
}
