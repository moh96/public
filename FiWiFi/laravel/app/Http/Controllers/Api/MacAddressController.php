<?php

namespace App\Http\Controllers\Api;

use App\MacAddress;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class MacAddressController extends BaseController
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'macAddress' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $macAddress = MacAddress::updateOrCreate(
            ['user_id' => Auth::user()->id], ['address' => $request['macAddress']]);

        $macAddress->save();

        $success['userId'] = Auth::user()->id;


        return $this->sendResponse(null, 'Mac Address stored');

    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $userId = $request['userId'];
        if (User::find($userId)) {
            $macAddress = User::find($userId)->macAddress;
            $data['macAddress'] = $macAddress->address;

            return $this->sendResponse($data, 'Mac Address retrieved');

        }
        return $this->sendError('Mac Address not found', null);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
