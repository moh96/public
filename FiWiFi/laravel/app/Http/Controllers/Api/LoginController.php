<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends BaseController
{
    public function login(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();
            $success['userId'] = $user->id;
            $success['userName'] = $user->name;
            $success['token'] = $user->createToken('Wifie')->accessToken;

            return $this->sendResponse($success, 'User login successfully.');

        } else {
            return $this->sendError('Incorrect email or password');
        }
    }
}
