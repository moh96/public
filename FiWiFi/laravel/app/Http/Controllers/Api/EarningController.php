<?php

namespace App\Http\Controllers\Api;

use App\Earning;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class EarningController extends BaseController
{
    public function get()
    {
        $user = User::find(Auth::id());
        $data = [];
        $earnings = [];

        if ($user) {
            foreach ($user->earnings as $earning) {
                $earn['clientName'] = User::find($earning->client_id)->name;
                $earn['amount'] = $earning->amount;
//                $earn['earnedAt'] = Carbon::parse($earning->earned_at)->format('M d h:ma');
                $earn['earnedAt'] = $earning->earned_at;
                $earnings[] = $earn;
            }

            $data['total'] = $user->earnings->sum('amount');
            $data['earnings'] = $earnings;
        }
        return $this->sendResponse($data, 'Success');
    }
}
