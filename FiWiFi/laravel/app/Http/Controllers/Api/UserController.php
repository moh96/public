<?php

namespace App\Http\Controllers\Api;

use App\MacAddress;
use App\Payment;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserController extends BaseController
{

    public function detailsByMacAddresses(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'macAddress' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $userNames = [];
        $payments = [];
        $macAddressArray = json_decode($request['macAddress']);

        for ($i = 0; $i < sizeof($macAddressArray); $i++) {
            $macAddress = MacAddress::where('address', $macAddressArray[$i])->first();
            if ($macAddress) {

                $user = $macAddress->user;
                $userNames[] = $user->name;

                $payment = Payment::where('client_id', $user->id)->where('provider_id', Auth::id())->first();

                if ($payment->paid) {
                    $payments[] = "Paid";
                }else{
                    $payments[] = "Unpaid";
                }
            } else {
                $userNames[] = "Unknown client";
                $payments[] = "None";
            }
        }

        $data['userNames'] = $userNames;
        $data['payments'] = $payments;

        return $this->sendResponse($data, 'Users found');

    }
}
