<?php

namespace App\Http\Controllers\Api;

use App\Earning;
use App\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class PaymentController extends BaseController
{
    public function update(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'provider_id' => 'required',
            'amount' => 'required',
            'paid' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $payment = Payment::updateOrCreate(
            ['client_id' => Auth::id(), 'provider_id' => $request['provider_id']],
            ['paid' => $request['paid'], 'amount' => $request['amount']]);

        $payment->save();
        $this->saveEarning(Auth::id(), $request['provider_id'], $request['amount'],$request['earned_at']);

        return $this->sendResponse(null, 'Payment updated');

    }

    private function saveEarning($clientId, $providerId, $amount,$earnedAt)
    {
        $earning = new Earning;
        $earning->client_id = $clientId;
        $earning->provider_id = $providerId;
        $earning->amount = $amount;
        $earning->earned_at = $earnedAt;
        $earning->save();
    }
}
