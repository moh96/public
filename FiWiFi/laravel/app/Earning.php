<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Earning extends Model
{
    protected $fillable = ['client_id','provider_id', 'amount'];

    public function client()
    {
        $this->belongsTo('User', 'client_id');
    }
    public function provider()
    {
        $this->belongsTo('User', 'provider_id');
    }
}
