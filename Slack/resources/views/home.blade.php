@extends('layouts.app')

@section('content')
    <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="#">
                        SE Factory
                    </a>
                </li>
                <li>
                    <a href="{{ route('createChannel') }}">Channels<i class="fas fa-plus-circle float-right mr-2 mt-1"
                                                                      style="font-size: 1.2em"></i></a>
                </li>

                @foreach($channels as $channel)
                    <li>
                        <a href="{{route('showChannel', $channel)}}"><i class="fas fa-lock"></i> {{$channel->name}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper" class="mt-5 mb-5">
            <div id="messagesLog" class="container-fluid">

                @foreach($messages as $message)

                    <br><span class='username'>{{$message->user->name}} </span>
                    <span class='time text-muted'>{{$message->created_at}}</span><br>{{$message->body}}

                @endforeach
            </div>

            <div id="messageInputDiv" class="fixed-bottom">
                <input id="messageInput" class="form-control input-lg" type="text"
                       placeholder="Enter your message here...">
            </div>

        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->

    <script>
{{--                @if(isset($channel))--}}
        var channelId = "{{$channelId}}";
                {{--@else--}}
        {{--var channelId = "{{$channelId}}";--}}
                {{--@endif--}}
        var username = "{{Auth::user()->name}}";
        var messageStoreUrl = "{{ route('messageStore') }}";
        var csrf_token = '{{ csrf_token() }}';
    </script>

@endsection
