@extends('layouts.app')

@section('content')

    <div class="container mt-lg-5">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Create new channel</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('storeChannel') }}">
                            @csrf

                            <div class="form-group row ">
                                <label for="email"
                                       class="col-sm-4 col-form-label text-md-right">Channel name</label>

                                <div class="col-md-8">
                                    <input id="channel" name="channel" type="text" required autofocus>

                                </div>
                            </div>

                            <div class="form-group row justify-content-center">
                                <button type="submit" class="btn btn-primary">Create</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
