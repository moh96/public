var messageInput = document.getElementById("messageInput");

// Execute a function when the user releases a key on the keyboard
messageInput.addEventListener("keyup", function (event) {
    event.preventDefault();
    if (event.keyCode === 13) {
        if (messageInput.value == "" || messageInput.value.trim() == "") {
            return;
        }
        send();
        storeMessageRequest();
        messageInput.value = "";

        window.scrollTo(0, document.body.scrollHeight);
        // // window.scrollTo(0,do/cument.querySelector("#page-content-wrapper").scrollHeight);
        // // var objDiv = document.getElementById("#messagesLog");
        // objDiv.scrollTop = objDiv.scrollHeight;
    }
});

var socket;

window.onload = function () {
    init();
    console.log("channelId"+channelId);

    window.scrollTo(0, document.body.scrollHeight);
};

function createSocket(host) {

    if ('WebSocket' in window)
        return new WebSocket(host);
    else if ('MozWebSocket' in window)
        return new MozWebSocket(host);

    throw new Error("No web socket support in browser!");
}

function init() {
    var host = "ws://localhost:1000"+channelId+"/"+channelId;

    try {
        socket = createSocket(host);
        console.log('WebSocket - status ' + socket.readyState);
        console.log('host ' + host);
        socket.onopen = function () {
            console.log("welcome");
        };
        socket.onmessage = function (msg) {
            log(msg.data);
        };
        socket.onclose = function () {
            console.log("disconnected");
        };
    }
    catch (ex) {
        log(ex);
    }
    document.getElementById("messageInput").focus();
}

function send() {
    var msgObject = {
        body: document.getElementById('messageInput').value,
        username: username,
        time: function timeNow() {
            var d = new Date();
            var options = {hour: '2-digit', minute: '2-digit'};
            return d.toLocaleString('en-US', options);
        }()
    };

    try {
        socket.send(JSON.stringify(msgObject));
    } catch (ex) {
        log(ex);
    }
}

function log(msg) {
    var msgObject = JSON.parse(msg);
    document.getElementById("messagesLog").innerHTML += "<br>" + "<span class='username'>" + msgObject.username + " </span>"
        + "<span class='time text-muted'>" + msgObject.time + " </span>" + "<br>" + msgObject.body;
}

function storeMessageRequest() {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            console.log(xhr.response);
        }
    };
    xhr.open("POST", messageStoreUrl, true);
    xhr.setRequestHeader('X-CSRF-Token', csrf_token);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send("messageBody=" + messageInput.value.toString()+"&channelId="+channelId.toString());
}


// function onkey(event) {
//     if (event.keyCode == 13) {
//         send();
//     }
// }

//
// // var slack = {
// //
// //
// //     chat: {
// //
// //         elements: {
// //             msg: document.getElementById('msg').value
// //         },
// //
// //         host: "ws://192.168.88.248:12345/chat",
// //
// //         init: function () {
// //             let socket;
// //             try {
// //                 socket = this.createSocket();
// //                 this.log('WebSocket - status ' + socket.readyState);
// //                 this.socket.onopen = function () {
// //                     chat.logger("Welcome");
// //                 };
// //                 socket.onmessage = function () {
// //                     chat.logger(this.msg.data);
// //                 };
// //                 socket.onclose = function () {
// //                     chat.logger("Disconnected");
// //                 };
// //             }
// //             catch (ex) {
// //                 this.log(ex);
// //             }
// //             document.getElementById("msg").focus();
// //         },
// //
// //         createSocket: function () {
// //
// //             if ('WebSocket' in window)
// //                 return new WebSocket(this.host);
// //             else if ('MozWebSocket' in window)
// //                 return new MozWebSocket(this.host);
// //
// //             throw new Error("No web socket support in browser!");
// //         },
// //         logger: function () {
// //             document.getElementById("log").innerHTML += "<br>" + this.msg;
// //         }
// //
// //     },
// //     send: function () {
// //
// //         try {
// //             this.socket.send("testtttt");
// //         } catch (ex) {
// //             this.log(ex);
// //         }
// //     }
// // };
