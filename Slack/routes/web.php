<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware(['auth'])->group(function () {

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/home', 'HomeController@index')->name('home');;
    Route::post('/comment', 'MessageController@store')->name('messageStore');
    Route::get('/channel/create', 'ChannelController@create')->name('createChannel');
    Route::post('/channel/store', 'ChannelController@store')->name('storeChannel');
    Route::get('/channel/{channel}', 'ChannelController@show')->name('showChannel');

});
