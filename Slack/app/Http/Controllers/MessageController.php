<?php

namespace Slack\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Slack\Member;
use Slack\Message;

class MessageController extends Controller
{
    public function store(Request $request)
    {
        $user_id = Auth::user()->id;
        $message = new Message;
        $message->user_id = $user_id;
        $message->channel_id = $request['channelId'];
        $message->body = $request['messageBody'];
        $message->save();

    }

}
