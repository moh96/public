<?php

namespace Slack\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Slack\Channel;
use Slack\Member;
use Slack\Message;

class ChannelController extends Controller
{
    public function create()
    {
        return view('channel/create');
    }

    public function store(Request $request)
    {
        $channel = new Channel;
        $channel->name = $request->input('channel');
        $channel->user_id = Auth::id();
        $channel->save();

        $member = new Member;
        $member->channel_name = $request->input('channel');
        $member->user_id = Auth::id();
        $member->save();

        $channels = Channel::all();
        $messages = Message::orderBy('created_at', 'asc')->get();;
        return view('home', ["messages" => $messages, 'channels' => $channels, 'channelId' => 1]);


    }

    public function show(Channel $channel)
    {
        $channels = Channel::all();
        $messages = Message::where('channel_id', $channel->id)->get();
        return view('home', ['messages' => $messages, 'channelId' => $channel->id, 'channels' => $channels]);
    }
}
