<?php

namespace Slack\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Slack\Channel;
use Slack\Message;

class HomeController extends Controller
{
    public function index()
    {
        $channels = Channel::all();

        $messages = Message::orderBy('created_at', 'asc')->get();;
        return view('home', ["messages" => $messages, 'channels' => $channels, 'channelId'=> 1]);
    }
}
