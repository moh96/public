<?php
/**
 * Created by PhpStorm.
 * User: maf
 * Date: 26/04/18
 * Time: 12:14 م
 */

namespace Slack\Http\Controllers;

use Devristo\Phpws\Server\UriHandler\ClientRouter;
use Devristo\Phpws\Server\WebSocketServer;
use React\EventLoop\Factory;
use Zend\Log\Logger;
use Zend\Log\Writer\Stream;

require_once "chat.php";

require_once "../../../vendor/autoload.php";

$loop = Factory::create();

// Create a logger which writes everything to the STDOUT
$logger = new Logger();
$writer = new Stream("php://output");
$logger->addWriter($writer);

for ($i = 1; $i <= 100; $i++) {
    // Create a WebSocket server
    $server = new WebSocketServer("tcp://0.0.0.0:1000{$i}", $loop, $logger);

    // Create a router which transfers all /chat connections to the ChatHandler class
    $router = new ClientRouter($server, $logger);
    // route /chat url

    $router->addRoute("#^/{$i}#i", new ChatHandler($logger));
    $server->bind();

}
//// route unmatched urls durring this demo to avoid errors
//$router->addRoute('#^(.*)$#i', new ChatHandlerForUnroutedUrls($logger));

// Start the event loop
$loop->run();
