<?php

namespace Slack;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    public function messages()
    {
        return $this->hasMany('Slack\Message')->orderBy('created_at', 'asc');
    }
}
