<?php

namespace Slack;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    public function channel()
    {
        return $this->belongsTo('Slack\Channel');
    }
}
