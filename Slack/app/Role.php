<?php

namespace Slack;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function user()
    {
        return $this->belongsToMany('Slack\User');
    }
}
