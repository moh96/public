<?php

namespace Slack;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function messages()
    {
        return $this->hasMany('Slack\Message')->orderBy('created_at','desc');
    }

    public function role()
    {
        return $this->hasOne('Slack\Role');
    }

    public function channels()
    {
        return $this->hasMany('Slack\Channel');
    }
}
