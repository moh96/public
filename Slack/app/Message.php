<?php

namespace Slack;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public function user()
    {
        return $this->belongsTo('Slack\User');
    }

    public function channel()
    {
        return $this->belongsTo('Slack\Channel');
    }
}

