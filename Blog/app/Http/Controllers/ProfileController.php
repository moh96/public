<?php

namespace Blog\Http\Controllers;

use Blog\Blog;
use Blog\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $blogs = User::find(Auth::id())->blogs;

        return view('profile', compact('blogs'));
    }
}
