@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                @foreach($blogs as $blog)
                    <div class="card mb-4">
                        <img class="card-img-top" src="http://placehold.it/750x300" alt="Card image cap">
                        <div class="card-body">
                            <h2 class="card-title">{{ $blog->title }}</h2>
                            <p class="card-text">{{ str_limit($blog->content, 200) }}</p>
                            <a href="{{ action('BlogController@show', $blog) }}" class="btn btn-primary">Read More →</a>
                        </div>
                        <div class="card-footer text-muted">
                            Posted on {{ $blog->created_at->format('M j, Y') }} by
                            <a href="#">{{$blog->find($blog->id)->user->name}}</a>
                        </div>
                    </div>
                @endforeach
                {{ $blogs->links()}}
            </div>
        </div>
    </div>

@endsection
