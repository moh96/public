<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware(['auth'])->group(function () {

    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index');
    Route::get('/posts/create', 'PostController@create')->name('create');
    Route::post('/posts/create', 'PostController@store');
    Route::post('/like','LikeController@update')->name('likeUpdate');
    Route::get('/{id}', 'UserController@show');
    Route::post('/comment', 'CommentController@store')->name('CommentStore');
});