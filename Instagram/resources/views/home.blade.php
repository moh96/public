@extends('layouts.app')

@section('content')
    <meta name="_token" content="{{ csrf_token() }}">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                @foreach($posts as $post)
                    <div class="card mb-4" data-post-id="{{ $post->id }}">
                        <img class="card-img-top" src="storage/images/{{$post->image}}" alt="Card image cap">
                        <div class="card-body">
                            <div class="pt-1 pb-1">

                                @if ( Auth::user()->likes()->where('post_id', $post->id)->first())
                                    <a class="text-dark" href="#"><i
                                                class="likeIcon fas text-danger fa-heart fa-2x"></i></a>
                                @else
                                    <a class="text-dark" href=""><i class="likeIcon far fa-heart fa-2x"></i></a>
                                @endif
                                <a class="text-dark" href=""><i class="commentIcon far fa-comment fa-2x ml-2"></i></a>
                            </div>
                            <div class="font-weight-bold text-dark">
                                @if ($post->likes->count() == 1)
                                    <span>{{  $post->likes->count() }} like</span>
                                @elseif ($post->likes->count() > 1)
                                    <span>{{  $post->likes->count() }} likes</span>
                                @else
                                    <span></span>
                                @endif
                            </div>
                            <a class="font-weight-bold text-dark"
                               href="{{action('UserController@show', $post->find($post->id)->user->id)}}">{{$post->find($post->id)->user->name}} </a>
                            <span>{{ $post->caption }}</span>
                            <div class="commentsDiv">
                                @foreach($post->find($post->id)->comments as $comment)
                                    <div class="row ml-0">
                                        <a class="font-weight-bold text-dark mr-1"
                                           href="{{action('UserController@show', $post->find($post->id)->user->id)}}">{{$comment->find($comment->id)->user->name}} </a>
                                        <span>{{ $comment->body }}</span>
                                    </div>
                                @endforeach
                            </div>
                            <div class="text-muted">{{ $post->created_at->format('g:ia M j, Y') }}</div>
                        </div>
                        <div class="commentForm card-body pb-0 pt-0 border-top">
                            <input name="commentInput" autocomplete="off" class="comment w-100 border-0"
                                   placeholder="Add a comment…">
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <script>
        var authName = '{{Auth::user()->name}}';
        var csrf_token = '{{ csrf_token() }}';
        var likeUrl = "{{ route('likeUpdate') }}";
        var commentUrl = "{{ route('CommentStore') }}";
    </script>
@endsection