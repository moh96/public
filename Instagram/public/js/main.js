window.onload = function () {
    like.clickListener();
    comment.clickListener();
    comment.keyupListener();
};

var like = {
    likeElement: document.getElementsByClassName("likeIcon"),
    textElement: function (clickedElement) {
        return clickedElement.parentNode.parentNode.parentNode.childNodes[2];
    },
    change: function (element) {
        if (element.classList.contains('far')) {
            element.classList.remove('far');
            element.classList.add('fas');
            element.classList.add('text-danger');
            this.changeLikeText('add', element);

        } else if (element.classList.contains('fas')) {
            element.classList.remove('fas');
            element.classList.remove('text-danger');
            element.classList.add('far');
            this.changeLikeText('remove', element);
        }
    },
    changeLikeText: function (funcName, element) {
        var countLikesElement = like.textElement(element).childNodes[0];
        var likeText = countLikesElement.innerHTML.split(" ");

        if ((countLikesElement.innerHTML === "" && funcName === "add")
            || countLikesElement.innerHTML === "2 likes" && funcName === "remove") {
            countLikesElement.innerHTML = "1 like";

        } else if (likeText[0] == 1 && funcName === "remove") {
            countLikesElement.innerHTML = "";

        } else if (likeText[0] > 0 && funcName === "add") {
            countLikesElement.innerHTML = ++likeText[0] + " " + "likes";

        } else if (likeText[0] > 2 && funcName === "remove") {
            countLikesElement.innerHTML = --likeText[0] + " " + "likes";
        }
    },
    clickListener: function () {
        for (var i = 0; i < this.likeElement.length; i++) {
            this.likeElement[i].addEventListener('click', function (event) {
                event.preventDefault();
                like.change(this);
                like.request(this);
            });
        }
    },
    request: function (clickedElement) {
        var postElement = clickedElement.parentNode.parentNode.parentNode.parentNode;
        var postId = postElement.dataset.postId;

        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                console.log(xhr.response);
            }
        };
        xhr.open("POST", likeUrl, true);
        xhr.setRequestHeader('X-CSRF-Token', csrf_token);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send("postId=" + postId);

    }

};

var comment = {
    commentElement: document.getElementsByClassName("commentIcon"),
    commentInputElement: document.getElementsByClassName('comment'),
    clickListener: function () {
        for (var i = 0; i < this.commentElement.length; i++) {
            this.commentElement[i].addEventListener('click', function (event) {
                event.preventDefault();
                this.parentNode.parentNode.parentNode.parentNode.childNodes[4].childNodes[0].focus();
            });
        }
    },
    keyupListener: function () {
        for (var i = 0; i < this.commentInputElement.length; i++) {
            this.commentInputElement[i].addEventListener('keyup', function (event) {
                if (event.keyCode === 13) {
                    var commentDiv = this.parentNode.parentNode.childNodes[2].childNodes[8];
                    comment.add(this, commentDiv);
                    comment.request(this);
                    this.value = "";
                }
            });
        }
    },
    add: function (element, parent) {
        var commentElement = document.createElement('div');
        commentElement.setAttribute('class', 'row ml-0');

        var html = '<a class="font-weight-bold text-dark mr-1"\n' +
            'href="{{action(\'UserController@show\', $post->find($post->id)->user->id)}}"> ' + authName + ' </a>\n' +
            '<span>' + element.value + '</span>';

        commentElement.innerHTML = html;
        parent.appendChild(commentElement);
    },
    request: function (element) {
        var postElement = element.parentNode.parentNode;
        var postId = postElement.dataset.postId;
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                console.log(xhr.response);
            }
        };
        xhr.open("POST", commentUrl, true);
        xhr.setRequestHeader('X-CSRF-Token', csrf_token);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send("commentBody=" + element.value.toString() + "&postId=" + postId);
    }
};
