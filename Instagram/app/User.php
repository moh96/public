<?php

namespace Instagram;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function posts()
    {
        return $this->hasMany('Instagram\Post')->orderBy('created_at','desc');
    }

    public function likes()
    {
        return $this->hasMany('Instagram\Like');
    }

    public function comments()
    {
        return $this->hasMany('Instagram\Comment')->orderBy('created_at','desc');
    }
}
