<?php

namespace Instagram\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Instagram\Comment;
use Instagram\Post;

class CommentController extends Controller
{
    public function store(Request $request)
    {
        $comment = new Comment;
        $comment->user_id = Auth::user()->id;
        $comment->post_id = $request['postId'];
        $comment->body = $request['commentBody'];
        $comment->save();

    }
}
