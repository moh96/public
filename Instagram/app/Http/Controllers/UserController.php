<?php

namespace Instagram\Http\Controllers;

use Illuminate\Http\Request;
use Instagram\User;

class UserController extends Controller
{

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $posts = User::findOrFail($id)->posts;

        return view('/home', ['posts' => $posts]);
    }

}
