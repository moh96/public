<?php

namespace Instagram\Http\Controllers;

use Instagram\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts/create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'caption' => 'required',
            'image' => 'required|mimes:jpeg,jpg,png|max:5000'
        ]);

        $imagePath = $request->file('image')->store('public/images');

        $post = new POST;
        $post->user_id = $request->user()->id;
        $post->caption = $request->input('caption');
        $post->image = basename($imagePath);
        $post->save();

        return redirect()->action('HomeController@index');

    }
}
