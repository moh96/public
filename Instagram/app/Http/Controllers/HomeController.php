<?php

namespace Instagram\Http\Controllers;

use Illuminate\Http\Request;
use Instagram\Post;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = POST::orderBy('created_at','desc')->get();;
        return view('home', ['posts' => $posts]);
    }
}
