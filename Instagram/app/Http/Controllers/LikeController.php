<?php

namespace Instagram\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Instagram\Like;
use Illuminate\Http\Request;

class LikeController extends Controller
{

    /**
     * @param Request $request
     */
    public function update(Request $request)
    {

        $user_id = Auth::user()->id;
        $post_id = $request['postId'];

        $liked = Like::where('user_id', $user_id)
            ->where('post_id', $post_id)->first();

        if ($liked) {
            $liked->delete();
        } else {
            $like = new Like;
            $like->user_id = $user_id;
            $like->post_id = $post_id;
            $like->save();
        }

    }

}
