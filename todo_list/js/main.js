var todo = {
    addItem: function (title, date, desc) {

        //get id from local storage
        var id = this.getId();

        var itemObject = {
            id: id,
            title: title,
            date: date,
            desc: desc
        }
        var itemslist = [];

        //add itemslist to local storage
        if (localStorage.getItem('itemslist') == null) {

            //push object to array
            itemslist.push(itemObject);

            localStorage.setItem('itemslist', JSON.stringify(itemslist));

        } else {
            //get itemsArray from local storage
            var itemsArray = JSON.parse(localStorage.getItem('itemslist'));
            console.log(itemsArray);

            //push object to array
            itemsArray.push(itemObject);

            // reset the localStorage

            localStorage.setItem('itemslist', JSON.stringify(itemsArray));
        }

    },

    removeItem: function (element) {
        var buttonId = element.getAttribute('id');

        //remove "removeBtn" from id
        var id = buttonId.replace('removeBtn', '');

        document.getElementById('item' + id).remove();

        //remove itemObject from local storage
        if (localStorage.getItem('itemslist') != null) {

            var itemsArray = JSON.parse(localStorage.getItem('itemslist'));

            //remove item from array
            for (var i = 0; i < itemsArray.length; i++) {
                if (itemsArray[i].id == id) {
                    itemsArray.splice(i, 1);
                }
            }
            console.log(id)
            localStorage.setItem('itemslist', JSON.stringify(itemsArray));

        }

    },

    addItemToHtml: function (title, date, desc, id) {

        var todoList = document.getElementById('todoList');
        var item = document.createElement('div');
        item.setAttribute('id', 'item' + id);
        item.setAttribute('class', 'item');


        var html =
            '<div class="leftItem">\n' +
            '    <h2>' + title + '</h2>\n' +
            '    <p>added: ' + date + '</p>\n' +
            '    <h4>' + desc + '</h4>\n' +
            '</div>\n' +
            '<div class="rightItem">\n' +
            '    <button id="removeBtn' + id + '" onclick="todo.removeItem(this)">X</button>\n' +
            '</div>';

        item.innerHTML = html;
        todoList.appendChild(item);
    },

    getId: function () {
        if (localStorage.getItem('id') == null) {
            return 0;
        } else {
            return localStorage.getItem('id');
        }

    },

    incrementId: function () {
        var id;
        //set id with value 0
        if (localStorage.getItem('id') == null) {
            id = 0;
            localStorage.setItem('id', id.toString());

        } else {
            // get id and increment by 1
            id = parseInt(localStorage.getItem('id'));
            id++;
            localStorage.setItem('id', id.toString());
        }
    },

    formatDate: function (date) {

        var days = [
            'Sunday', 'Monday', 'Tuesday',
            'Wednesday', 'Thursday', 'Friday', 'Saturday'
        ];

        var months = [
            "January", "February", "March",
            "April", "May", "June", "July",
            "August", "September", "October",
            "November", "December"
        ];

        var dayName = days[date.getDay()];
        var monthName = months[date.getMonth()];
        var monthDay = date.getDate();
        var year = date.getFullYear();
        var time = date.getHours() + ':' + date.getMinutes();

        return dayName + ', ' + monthName + ' ' + monthDay + ' ' + year + ' ' + time;
    },

    isEmpty: function (input) {
        return (!input || 0 === input.length || !input.trim());
    }

}

document.getElementById("addButton").addEventListener("click", function () {

    var title = document.getElementById('itemTitle').value;
    var desc = document.getElementById('itemDescription').value;

    if (todo.isEmpty(title) || todo.isEmpty(desc)) {
        return false;
    }
    var date = new Date();
    date = todo.formatDate(date);
    todo.addItem(title, date, desc);
    todo.addItemToHtml(title, date, desc, todo.getId());
    todo.incrementId();
});

window.onload = function load() {
    var itemsList = localStorage.getItem('itemslist');
    if (localStorage.getItem('itemslist') != null) {
        var itemsArray = JSON.parse(itemsList);

        console.log(itemsArray);

        for (var i = 0; i < itemsArray.length; i++) {
            todo.addItemToHtml(itemsArray[i].title, itemsArray[i].date, itemsArray[i].desc, itemsArray[i].id);
        }
    }
}
